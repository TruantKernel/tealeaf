package usa.kerby.tk.tealeaf;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.ws.rs.core.Variant;

import com.jayway.jsonpath.JsonPath;

import usa.kerby.tk.jchal.ChaloEntity;
import usa.kerby.tk.jchal.http.HttpRequest;
import usa.kerby.tk.jchal.http.HttpResponse;
import usa.kerby.tk.jhal.Hal;
import usa.kerby.tk.jhal.NonProp;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.http.LinkRelation;
import usa.kerby.tk.tealeaf.bem.BemItem;
import usa.kerby.tk.tealeaf.bem.BemJson;
import usa.kerby.tk.tealeaf.bem.Block;

/**
 * @author Trevor Kerby
 * @since Jul 9, 2020
 */

@JsonbTypeSerializer(TealeafSerializer.class)
public class TealeafEntity extends ChaloEntity implements TealeafRepresentation {

	public static class Builder extends ChaloEntity.Builder implements Tealeaf {
		protected List<BemItem> bem;

		// public Builder(URI self, Variant variant, String blockName, String path,
		// DataTransferObject... dto) {
		// super(self, variant, path, dto);
		// this.bem = new ArrayList<BemItem>();
		// this.bem.add(new BemItem(new
		// BemJson.Builder(blockName).path(JsonPath.compile(this.getPath() + "." +
		// TealeafEntity.BEM_PROPERTY_NAME))));
		// // + "[0]"
		// }

		public Builder(URI self, Variant variant, String blockName, DataTransferObject... dto) {
			super(self, variant, dto);
			this.bem = new ArrayList<BemItem>();
			this.bem.add(new BemItem(new BemJson.Builder(blockName)
					.path(JsonPath.compile(this.getPath() + "." + TealeafEntity.BEM_PROPERTY_NAME)))); // +
																										// "[0]"
		}

		@Override
		public TealeafEntity.Builder addEmbedded(LinkRelation rel, Hal builder) {
			if (this.embedded.get(rel) == null) {
				this.embedded.put(rel, new ArrayList<Hal>());
			}
			// if (this.getPath() != null && ((TealeafEntity.Builder) builder).getPath() ==
			// null) {
			// builder = ((HaloEntity.Builder) builder)
			// .withPath(this.getPath() + "." + HaloEntity.EMBEDDED_PROPERTY_NAME + "." +
			// rel + "[" + this.embedded.get(rel).size() + "]");
			// }
			this.embedded.get(rel).add(builder);
			HttpRequest req = new HttpRequest.Builder(builder.getUri(), this.method).build();
			HttpResponse resp = new HttpResponse.Builder(this.variant).build();
			return (Builder) ((TealeafEntity.Builder) builder).withReponse(resp).withRequest(req);
		}

		@Override
		public TealeafEntity.Builder addEmbedded(LinkRelation rel, URI self, DataTransferObject... dto) {
			HttpRequest req = new HttpRequest.Builder(self, this.method).build();
			HttpResponse resp = new HttpResponse.Builder(this.variant).build();
			return this.addEmbedded(rel, self, req, resp, dto);
		}

		public TealeafEntity.Builder addEmbedded(LinkRelation rel, URI self, HttpRequest request, HttpResponse response,
				DataTransferObject... dto) {
			String blockName = (rel instanceof LinkRelation) ? ((LinkRelation) rel).getName() : rel.toString();
			return this.addEmbedded(rel, new TealeafEntity.Builder(self, variant, blockName, dto).withReponse(response)
					.withRequest(request));
		}

		@Override
		public TealeafEntity build() {
			TealeafEntity halo = new TealeafEntity(this);
			return halo;
		}

		@Override
		@NonProp
		public TealeafEntity.Builder getEmbedded(LinkRelation rel, int index) {
			return (TealeafEntity.Builder) embedded.get(rel).get(index);
		}

		@NonProp
		public JsonPath getPathToBem() {
			return JsonPath.compile(this.getPath() + "." + TealeafEntity.BEM_PROPERTY_NAME);
		}

		@Override
		@NonProp
		public PathDecorator<Object> getProp(String key) {
			return this.getProps().get(key);
		}

		@Override
		@NonProp
		public Map<String, PathDecorator<Object>> getProps() {
			return props;
		}

		@Override
		@NonProp
		public Block getBem(int index) {
			BemItem item = this.bem.get(index);
			if (item.getBuilder() instanceof Block) {
				return item.getBuilder();
			} else {
				return null;
			}
		}

		// private String nextEmbeddedPath(LinkRelation rel) {
		// final int index = (this.embedded.containsKey(rel)) ?
		// this.embedded.get(rel).size() : 0;
		// return this.getPath() + "." + HaloEntity.EMBEDDED_PROPERTY_NAME + "." + rel +
		// "[" + index + "]";
		// }

		protected Map<String, PathDecorator<Object>> registerProps(Map<String, PathDecorator<Object>> props) {
			List<Method> methods = new ArrayList<Method>(Arrays.asList(this.getClass().getDeclaredMethods()));
			for (Method m : methods) {
				if (m.getName().startsWith("get") && !m.isAnnotationPresent(NonProp.class) && !m.isSynthetic()
						&& !m.isBridge()) {
					try {
						String name = m.getName().substring(3).toLowerCase();
						props.put(name, new PathDecorator<Object>(m.invoke(this), this.getPath() + "." + name));
					} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
			return props;
		}

		@Override
		protected Builder self() {
			return this;
		}

		@Override
		public Tealeaf addBem(Block block) {
			this.bem.add(new BemItem(block));
			return this;
		}

		@Override
		public Tealeaf linkBem(Tealeaf tealeaf) {
			for (Entry<LinkRelation, List<Hal>> entry : this.getEmbedded().entrySet()) {
				List<Hal> halos = new ArrayList<Hal>(entry.getValue());
				for (int i = 0; i < halos.size(); i++) {
					if (halos.get(i).equals(tealeaf)) {
						this.bem.add(new BemItem(JsonPath.compile((this.path + "." + EMBEDDED_PROPERTY_NAME + "."
								+ entry.getKey() + "[" + i + "]" + TealeafEntity.BEM_PROPERTY_NAME))));
					}
				}
			}
			return this;
		}

		@Override
		public Tealeaf linkAllBem() {
			for (Entry<LinkRelation, List<Hal>> entry : this.getEmbedded().entrySet()) {
				List<Hal> halos = new ArrayList<Hal>(entry.getValue());
				for (int i = 0; i < halos.size(); i++) {
					this.bem.add(new BemItem(JsonPath.compile((this.path + "." + EMBEDDED_PROPERTY_NAME + "."
							+ entry.getKey() + "[" + i + "]" + TealeafEntity.BEM_PROPERTY_NAME))));
				}
			}
			return this;
		}

		public Tealeaf linkBemAsContent(Tealeaf tealeaf, Block dest) {
			for (Entry<LinkRelation, List<Hal>> entry : this.getEmbedded().entrySet()) {
				List<Hal> halos = new ArrayList<Hal>(entry.getValue());
				for (int i = 0; i < halos.size(); i++) {
					if (halos.get(i).equals(tealeaf)) {
						dest.addContent(JsonPath.compile((this.path + "." + EMBEDDED_PROPERTY_NAME + "."
								+ entry.getKey() + "[" + i + "]" + TealeafEntity.BEM_PROPERTY_NAME)));
					}
				}
			}
			return this;
		};

		public Tealeaf linkAllBemAsContent(Block dest) {
			for (Entry<LinkRelation, List<Hal>> entry : this.getEmbedded().entrySet()) {
				List<Hal> halos = new ArrayList<Hal>(entry.getValue());
				for (int i = 0; i < halos.size(); i++) {
					dest.addContent(JsonPath.compile((this.path + "." + EMBEDDED_PROPERTY_NAME + "." + entry.getKey()
							+ "[" + i + "]" + TealeafEntity.BEM_PROPERTY_NAME)));
				}
			}
			return this;
		};

	}

	public static final String BEM_PROPERTY_NAME = "_bem";

	protected final List<BemItem> bem;

	protected TealeafEntity(Builder builder) {
		super(builder);
		this.bem = new ArrayList<BemItem>(builder.bem.size());
		for (BemItem block : builder.bem) {
			if (block.isReference()) {
				this.bem.add(new BemItem(block.getPath()));
			} else {
				this.bem.add(new BemItem((BemJson) block.getBuilder().build()));
			}
		}
	}

	@JsonbTransient
	public BemItem getBem(int i) {
		return this.bem.get(i);
	}

	@JsonbTransient
	public List<BemItem> getBem() {
		return this.bem;
	}

	@Override
	@JsonbTransient
	public PathDecorator<Object> getProp(String key) {
		return this.getProps().get(key);
	}

	@Override
	@JsonbTransient
	public Map<String, PathDecorator<Object>> getProps() {
		return props;
	}

}
