package usa.kerby.tk.tealeaf.bem;

import com.jayway.jsonpath.JsonPath;


/**
 * @author Trevor Kerby
 * @since Aug 30, 2020
 */
public class BemItem {

	private final Block builder;
	private final Bem bem;
	private final JsonPath path;

	public BemItem(Block builder) {
		this.builder = builder;
		this.bem = null;
		this.path = null;
	}

	public BemItem(Bem bem) {
		this.bem = bem;
		this.builder = null;
		this.path = null;
	}

	public BemItem(JsonPath path) {
		this.path = path;
		this.bem = null;
		this.builder = null;
	}

	public boolean isReference() {
		return (this.path != null) ? true : false;
	}

	/**
	 * @return the builder
	 */
	public Block getBuilder() {
		return builder;
	}

	/**
	 * @return the bem
	 */
	public Bem getBemJson() {
		return bem;
	}

	/**
	 * @return the path
	 */
	public JsonPath getPath() {
		return path;
	}

}
