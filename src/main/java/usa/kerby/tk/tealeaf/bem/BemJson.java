package usa.kerby.tk.tealeaf.bem;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.bind.annotation.JsonbTypeSerializer;

import com.jayway.jsonpath.JsonPath;

import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.tealeaf.html.HtmlAttribute;
import usa.kerby.tk.tealeaf.html.RegisteredHtmlAttribute;

/**
 * @author Trevor Kerby
 * @since Jul 9, 2020
 * 
 */

@JsonbTypeSerializer(BemSerializer.class)
public class BemJson implements Bem {

	private final JsonPath path;
	private final String block;
	private final String elem;
	private final Map<String, String> mods;
	private final Map<String, String> elemMods;
	private final List<BemContent> content; // TODO figure out this mess
	private final List<BemJson> mix;
	private final JavascriptObject js;
	private final Boolean bem;
	private final BemHtmlAttributes attributes;
	private final String cls;
	private final String tag;

	public static class Builder implements Block {

		private JsonPath path;
		private String block;
		private String elem;
		private Map<String, String> mods;
		private Map<String, String> elemMods;
		private List<BemContent> content;
		private List<Block> mix;
		private JavascriptObject.Builder js;
		private boolean bem;
		private BemHtmlAttributes.Builder attributes;
		private String cls;
		private String tag;

		public Builder() {
			this.bem = true;
		}

		public Builder(String blockname) {
			this.block = blockname;
			this.bem = true;
		}

		@Override
		public Builder addAttribute(HtmlAttribute key, String value) {
			if (this.attributes == null) {
				this.attributes = new BemHtmlAttributes.Builder();
			}
			this.attributes.addAttribute(key, value);
			return this;
		}

		@Override
		public Builder attributes(BemHtmlAttributes.Builder attributes) {
			this.attributes = attributes;
			return this;
		}

		@Override
		public Builder bem(boolean bem) {
			this.bem = bem;
			return this;
		}

		@Override
		public Builder blockName(String block) {
			this.block = block;
			return this;
		}

		@Override
		public Builder cls(String cls) {
			this.cls = cls;
			return this;
		}

		@Override
		public Builder content(List<BemContent> content) {
			this.content = content;
			return this;
		}

		@Override
		public Builder elem(String elem) {
			this.elem = elem;
			return this;
		}

		@Override
		public Builder elemMods(Map<String, String> elemMods) {
			this.elemMods = elemMods;
			return this;
		}

		@Override
		public Builder js(JavascriptObject.Builder js) {
			this.js = js;
			return this;
		}

		@Override
		public Builder mix(List<Block> mix) {
			this.mix = mix;
			return this;
		}

		@Override
		public Builder mods(Map<String, String> mods) {
			this.mods = mods;
			return this;
		}

		@Override
		public Builder addObjectAttribute(HtmlAttribute object, String key, String value) {
			if (this.attributes == null) {
				this.attributes = new BemHtmlAttributes.Builder();
			}
			this.attributes.addObjectAttribute(object, key, value);
			return this;
		}

		@Override
		public Block addContent(Block content) {
			if (this.content == null) {
				this.content = new ArrayList<BemContent>();
			}
			this.content.add(new BemContent.Builder().bem((Builder) content));
			return this;
		}

		@Override
		public Block addContent(JsonPath path) {
			if (this.content == null) {
				this.content = new ArrayList<BemContent>();
			}
			this.content.add(new BemContent.Builder().ref(path));
			return this;
		}

		@Override
		public Block addContent(PathDecorator<?> pathed) {
			this.addContent(pathed.getPath());
			return this;
		}

		@Override
		public Block addContent(String content) {
			if (this.content == null) {
				this.content = new ArrayList<BemContent>();
			}
			this.content.add(new BemContent.Builder().text(content));
			return this;
		}

		@Override
		public Builder tag(String tag) {
			this.tag = tag;
			return this;
		}

		@Override
		public Builder path(JsonPath path) {
			this.path = path;
			return this;
		}

		/**
		 * @return
		 */
		@Override
		public BemJson build() {
			return new BemJson(this);
		}

		@Override
		public JsonPath getPath() {
			return this.path;
		}

		@Override
		public Block addMix(Block bem) {
			if (this.mix == null) {
				this.mix = new ArrayList<Block>();
			}
			this.mix.add(bem);
			return this;
		}

		@Override
		public Block addMod(String key, String value) {
			if (this.mods == null) {
				this.mods = new HashMap<String, String>();
			}
			this.mods.put(key, value);
			return this;
		}

		@Override
		public Block addElemMod(String key, String value) {
			if (this.elemMods == null) {
				this.elemMods = new HashMap<String, String>();
			}
			this.elemMods.put(key, value);
			return this;
		}
	}

	public BemJson(Builder builder) {
		this.path = builder.path;
		this.block = builder.block;
		this.elem = builder.elem;
		this.mods = builder.mods;
		this.elemMods = builder.elemMods;
		this.content = builder.content;
		this.mix = BemJson.buildMix(builder.mix);
		this.js = (builder.js != null) ? builder.js.build() : null;
		this.bem = builder.bem;
		this.attributes = (builder.attributes != null) ? builder.attributes.build() : null;
		this.cls = builder.cls;
		this.tag = builder.tag;
	}

	private static List<BemJson> buildMix(List<Block> unbuilt) {
		if (unbuilt != null && !unbuilt.isEmpty()) {
			List<BemJson> list = new ArrayList<BemJson>(unbuilt.size());
			unbuilt.forEach((s) -> list.add(((Builder) s).build()));
			return Collections.unmodifiableList(list);
		}
		return null;
	}

	// @JsonbTypeAdapter(BemHtmlAttributesAdapter.class)
	public BemHtmlAttributes getAttributes() {
		return this.attributes;
	}

	public String getBlock() {
		return this.block;
	}

	public String getCls() {
		return this.cls;
	}

	public List<BemContent> getContent() {
		return this.content;
	}

	public String getElem() {
		return this.elem;
	}

	public Map<String, String> getElemMods() {
		return this.elemMods;
	}

	public JavascriptObject getJs() {
		return this.js;
	}

	public List<BemJson> getMix() {
		return this.mix;
	}

	public Map<String, String> getMods() {
		return this.mods;
	}

	public String getTag() {
		return this.tag;
	}

	public boolean isBem() {
		return this.bem;
	}

	/**
	 * @return the path
	 */
	public JsonPath getPath() {
		return this.path;
	}

	public static BemJson.Builder createLinkBlock(JsonPath path) {
		return new BemJson.Builder("link").addObjectAttribute(RegisteredHtmlAttribute.HREF, "$ref", path.getPath());
	}

	public static BemJson.Builder createLinkBlock(URI uri) {
		return new BemJson.Builder("link").addAttribute(RegisteredHtmlAttribute.HREF, uri.toASCIIString());
	}

}
