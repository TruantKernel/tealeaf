package usa.kerby.tk.tealeaf.bem;

import java.util.Map;
import java.util.Map.Entry;

import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

import com.jayway.jsonpath.JsonPath;

import usa.kerby.tk.tealeaf.html.HtmlAttribute;

/**
 * @author Trevor Kerby
 * @since Jul 18, 2020
 */

@JsonbTypeSerializer(BemSerializer.class)
public class BemSerializer implements JsonbSerializer<BemJson> {

	@Override
	public void serialize(BemJson bem, JsonGenerator generator, SerializationContext ctx) {
		this.serializeBem(bem, generator, ctx);
	}

	public void serializeBem(BemJson bem, JsonGenerator generator, SerializationContext ctx) {
		if (bem.getElem() != null) {
			ctx.serialize("elem", bem.getElem(), generator);
			ctx.serialize("elemMods", bem.getElemMods(), generator);
		} else {
			ctx.serialize("block", bem.getBlock(), generator);
			ctx.serialize("mods", bem.getMods(), generator);
		}
		if (bem.getMix() != null && !bem.getMix().isEmpty()) {
			if (bem.getMix().size() == 1) {
				generator.writeStartObject("mix");
				ctx.serialize(bem.getMix().get(0), generator);
				generator.writeEnd();
			} else {
				ctx.serialize("mix", bem.getMix(), generator);
			}
		}
		ctx.serialize("js", bem.getJs(), generator);
		if (!bem.isBem()) {
			ctx.serialize("bem", false, generator);
		}
		if (bem.getAttributes() != null) {
			generator.writeStartObject("attributes");
			for (Map.Entry<HtmlAttribute, String> entry : bem.getAttributes().getAttributes().entrySet()) {
				generator.write(entry.getKey().toString(), entry.getValue());
			}
			for (Entry<HtmlAttribute, Map<String, String>> entry : bem.getAttributes().getObjectAttributes().entrySet()) {
				generator.writeStartObject(entry.getKey().toString());
				for (Map.Entry<String, String> innerEntry : entry.getValue().entrySet()) {
					generator.write(innerEntry.getKey(), innerEntry.getValue());
				}
				generator.writeEnd();
			}
			generator.writeEnd();
		}
		ctx.serialize("cls", bem.getCls(), generator); // String
		ctx.serialize("tag", bem.getTag(), generator); // String
		if (bem.getContent() != null) {
			if (!bem.getContent().isEmpty()) {
				generator.writeStartArray("content");
				for (BemContent content : bem.getContent()) {
					if (content.getBem() != null) {
						generator.writeStartObject();
						this.serializeBem(content.getBem(), generator, ctx);
						generator.writeEnd();
					} else if (content.getRef() != null) {
						this.linkRef(generator, content.getRef());
					} else if (content.getText() != null) {
						ctx.serialize(content.getText(), generator);
					} else if (content.getHtml() != null) {
						ctx.serialize(content.getHtml(), generator);
					}
				}
				generator.writeEnd();
			}
			// } else if (bem.getTextContent() != null) {
			// ctx.serialize("content", bem.getTextContent(), generator);
		}
	}

	private void linkRef(JsonGenerator generator, JsonPath path) {
		generator.writeStartObject();
		generator.write("$ref", path.getPath());
		generator.writeEnd();
	}

}
