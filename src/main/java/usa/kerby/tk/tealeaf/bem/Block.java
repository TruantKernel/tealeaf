package usa.kerby.tk.tealeaf.bem;

import java.util.List;
import java.util.Map;

import com.jayway.jsonpath.JsonPath;

import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhal.jsonpath.Pathable;
import usa.kerby.tk.tealeaf.html.HtmlAttribute;

/**
 * @author Trevor Kerby
 * @since Aug 5, 2020
 */
public interface Block extends Pathable {
	public Block addAttribute(HtmlAttribute key, String value);

	public Block attributes(BemHtmlAttributes.Builder attributes);

	public Block bem(boolean bem);

	public Block blockName(String block);

	public Block cls(String cls);

	public Block content(List<BemContent> content);

	public Block elem(String elem);

	public Block elemMods(Map<String, String> elemMods);

	public Block js(JavascriptObject.Builder js);

	public Block mix(List<Block> mix);

	public Block mods(Map<String, String> mods);

	public Block addObjectAttribute(HtmlAttribute object, String key, String value);

	public Block addContent(Block content);

	public Block addContent(JsonPath path);

	public Block addContent(PathDecorator<?> pathed);

	public Block addContent(String content);

	public Block addMix(Block bem);

	public Block addMod(String key, String value);

	public Block addElemMod(String key, String value);

	public Block tag(String tag);

	public Block path(JsonPath path);

	public JsonPath getPath();

	/**
	 * @return
	 */
	public Bem build();
}
