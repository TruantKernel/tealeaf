package usa.kerby.tk.tealeaf.bem;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import usa.kerby.tk.tealeaf.html.HtmlAttribute;

/**
 * @author Trevor Kerby
 * @since Jul 10, 2020
 */
public final class BemHtmlAttributes {
	private final Map<HtmlAttribute, Map<String, String>> objectAttributes;
	private final Map<HtmlAttribute, String> attributes;

	public static class Builder {
		private Map<HtmlAttribute, Map<String, String>> objectAttributes;
		private Map<HtmlAttribute, String> attributes;

		public Builder() {
			this.attributes = new HashMap<HtmlAttribute, String>();
			this.objectAttributes = new HashMap<HtmlAttribute, Map<String, String>>();
		}

		public Builder addObject(HtmlAttribute object, Map<String, String> attributes) {
			this.objectAttributes.put(object, attributes);
			return this;
		}

		public Builder removeObject(HtmlAttribute object) {
			this.objectAttributes.remove(object);
			return this;
		}

		public Builder addAttribute(HtmlAttribute key, String value) {
			this.attributes.put(key, value);
			return this;
		}

		public Builder removeAttribute(HtmlAttribute attribute) {
			this.attributes.remove(attribute);
			return this;
		}

		public Builder addObjectAttribute(HtmlAttribute object, String key, String value) {
			if (!this.objectAttributes.containsKey(object)) {
				this.objectAttributes.put(object, new HashMap<String, String>());
			}
			this.objectAttributes.get(object).put(key, value);
			return this;
		}

		public Builder removeObjectAttribute(HtmlAttribute object, String key) {
			if (this.objectAttributes.containsKey(object)) {
				this.objectAttributes.get(object).remove(key);
			}
			return this;
		}

		public BemHtmlAttributes build() {
			return new BemHtmlAttributes(this);
		}

	}

	public BemHtmlAttributes(Builder builder) {
		// this.objectAttributes = (builder.objectAttributes != null) ? Collections.unmodifiableMap(builder.objectAttributes) : null;
		this.objectAttributes = BemHtmlAttributes.buildObjectAttributes(builder.objectAttributes);
		this.attributes = (builder.attributes != null) ? Collections.unmodifiableMap(builder.attributes) : null;
	}

	private static Map<HtmlAttribute, Map<String, String>> buildObjectAttributes(Map<HtmlAttribute, Map<String, String>> objectAttributes) {
		Map<HtmlAttribute, Map<String, String>> map = new HashMap<HtmlAttribute, Map<String, String>>();
		for (Map.Entry<HtmlAttribute, Map<String, String>> entry : objectAttributes.entrySet()) {
			map.put(entry.getKey(), Collections.unmodifiableMap(entry.getValue()));
		}
		return map;
	}

	public Map<HtmlAttribute, Map<String, String>> getObjectAttributes() {
		return objectAttributes;
	}

	public Map<HtmlAttribute, String> getAttributes() {
		return attributes;
	}

}
