package usa.kerby.tk.tealeaf.bem;

import org.w3c.dom.html.HTMLElement;

import com.jayway.jsonpath.JsonPath;

/**
 * @author Trevor Kerby
 * @since Aug 3, 2020
 */
public class BemContent {

	private final BemJson bem;
	private final JsonPath ref;
	private final String text;
	private final HTMLElement html;

	public static class Builder {
		private BemJson.Builder bem;
		private JsonPath ref;
		private String text;
		private HTMLElement html;

		public BemContent bem(BemJson.Builder content) {
			this.bem = content;
			return new BemContent(this);
		}

		public BemContent ref(JsonPath content) {
			this.ref = content;
			return new BemContent(this);
		}

		public BemContent text(String content) {
			this.text = content;
			return new BemContent(this);
		}

		public BemContent html(HTMLElement content) {
			this.html = content;
			return new BemContent(this);
		}
	}

	/**
	 * @param builder
	 */
	public BemContent(Builder builder) {
		this.bem = (builder.bem != null) ? builder.bem.build() : null;
		this.text = builder.text;
		this.ref = builder.ref;
		this.html = builder.html;
	}

	public BemJson getBem() {
		return bem;
	}

	public JsonPath getRef() {
		return ref;
	}

	public String getText() {
		return text;
	}

	public HTMLElement getHtml() {
		return html;
	}

}
