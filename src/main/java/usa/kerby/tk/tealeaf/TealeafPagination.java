package usa.kerby.tk.tealeaf;

import java.net.URI;

import javax.json.bind.annotation.JsonbTypeSerializer;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Variant;

import usa.kerby.tk.jhal.NonProp;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.Pagination;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;
import usa.kerby.tk.jhateoas.http.RegisteredLinkRelation;
import usa.kerby.tk.pageable.Pageable;
import usa.kerby.tk.tealeaf.bem.BemJson;

/**
 * @author Trevor Kerby
 * @since Jul 17, 2020
 */
@JsonbTypeSerializer(TealeafSerializer.class)
public final class TealeafPagination extends TealeafEntity implements Pageable, TealeafRepresentation {

	public final static class Builder extends TealeafEntity.Builder implements Pageable, Tealeaf {
		private int offset;
		private int limit;
		private int size;

		public Builder(URI uri, Variant variant, int size, int offset, int limit) {
			super(uri, variant, "pagination", (DataTransferObject) null);
			this.uri = uri;
			this.offset = offset;
			this.size = size;
			this.limit = limit;
			this.props = this.registerProps(this.props);
		}

		public Builder(UriInfo uri, Variant variant, int size, int offset, int limit) {
			this(uri.getAbsolutePath(), variant, size, offset, limit);
		}

		public Builder(URI uri, Variant variant, Pagination page) {
			this(uri, variant, page.size, page.offset, page.limit);
		}

		public Builder(UriInfo uri, Variant variant, Pagination page) {
			this(uri, variant, page.size, page.offset, page.limit);
		}

		@Override
		public TealeafPagination build() {
			this.createPageLinks();
			return new TealeafPagination(this);
		}

		/**
		 * @param rel
		 * @return
		 */
		private BemJson.Builder createLinkBlock(LinkRelation rel) {
			return BemJson.createLinkBlock(this.links.get(rel).getPath());
		}

		private BemJson.Builder createPageLink(LinkRelation rel, int offset, int limit) {
			BemJson.Builder itemBlock = new BemJson.Builder("controls").elem("item");
			this.addLink(rel, Hyperlink.createPaginationHyperlink(this.uri, offset, limit));
			BemJson.Builder linkBlock = this.createLinkBlock(rel);
			linkBlock.addContent(rel.toString());
			itemBlock.addContent(linkBlock);
			return itemBlock;
		}

		private BemJson.Builder createPageLinks() {
			BemJson.Builder root = new BemJson.Builder("controls");
			if (this.continuesBackward()) {
				root.addContent(this.createPageLink(RegisteredLinkRelation.FIRST, this.firstPage(), this.limit));
				root.addContent(this.createPageLink(RegisteredLinkRelation.PREV, this.previousPage(), this.limit));
			}
			if (this.continuesForward()) {
				root.addContent(this.createPageLink(RegisteredLinkRelation.NEXT, this.nextPage(), this.limit));
				root.addContent(this.createPageLink(RegisteredLinkRelation.LAST, this.lastPage(), this.limit));
			}
			return root;
		}

		@Override
		public int getLimit() {
			return this.limit;
		}

		@Override
		public int getOffset() {
			return this.offset;
		}

		@Override
		public int getSize() {
			return this.size;
		}

		@Override
		protected Builder self() {
			return this;
		}

		public Builder limit(int limit) {
			this.limit = limit;
			return this;
		}

		public Builder offset(int offset) {
			this.offset = offset;
			return this;
		}

		public Builder size(int size) {
			this.size = size;
			return this;
		}

		@Override
		@NonProp
		public TealeafEntity.Builder getEmbedded(LinkRelation rel, int index) {
			return (TealeafEntity.Builder) embedded.get(rel).get(index);
		}

	}

	private final Integer offset;
	private final Integer limit;

	private final Integer size;

	public TealeafPagination(TealeafPagination.Builder builder) {
		super(builder);
		this.offset = builder.offset;
		this.limit = builder.limit;
		this.size = builder.size;
	}

	@Override
	public int getLimit() {
		return this.limit;
	}

	@Override
	public int getOffset() {
		return this.offset;
	}

	@Override
	public int getSize() {
		return this.size;
	}

}
