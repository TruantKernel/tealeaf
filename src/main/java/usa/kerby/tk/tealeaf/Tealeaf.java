package usa.kerby.tk.tealeaf;

import java.net.URI;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import usa.kerby.tk.jchal.Chal;
import usa.kerby.tk.jhal.Hal;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.DataTransferObject;
import usa.kerby.tk.jhateoas.http.LinkRelation;
import usa.kerby.tk.tealeaf.bem.Block;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface Tealeaf extends Chal {

	public static final String APPLICATION_JSON_TEALEAF = "application/vnd.tealeaf.tealeaf+json";
	public static final MediaType APPLICATION_JSON_TEALEAF_TYPE = new MediaType("application",
			"vnd.tealeaf.tealeaf+json");

	public static final String APPLICATION_XML_TEALEAF = "application/vnd.tealeaf.tealeaf+xml";
	public static final MediaType APPLICATION_XML_TEALEAF_TYPE = new MediaType("application",
			"vnd.tealeaf.tealeaf+xml");

	@Override
	public String getPath();

	@Override
	public Tealeaf addEmbedded(LinkRelation rel, Hal builder);

	@Override
	public Tealeaf addEmbedded(LinkRelation rel, URI self, DataTransferObject... dto);

	@Override
	public TealeafRepresentation build();

	@Override
	public Tealeaf getEmbedded(LinkRelation rel, int index);

	public PathDecorator<Object> getProp(String key);

	@Override
	public Map<String, PathDecorator<Object>> getProps();

	public Block getBem(int index);

	public Tealeaf addBem(Block block);

	public Tealeaf linkBem(Tealeaf tealeaf);

	public Tealeaf linkAllBem();

	public Tealeaf linkBemAsContent(Tealeaf tealeaf, Block dest);

	public Tealeaf linkAllBemAsContent(Block dest);
}
