package usa.kerby.tk.tealeaf;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.bind.serializer.JsonbSerializer;
import javax.json.bind.serializer.SerializationContext;
import javax.json.stream.JsonGenerator;

import usa.kerby.tk.jchal.http.HttpRequest;
import usa.kerby.tk.jchal.http.HttpResponse;
import usa.kerby.tk.jhal.HalRepresentation;
import usa.kerby.tk.jhal.jsonpath.PathDecorator;
import usa.kerby.tk.jhateoas.http.Hyperlink;
import usa.kerby.tk.jhateoas.http.LinkRelation;
import usa.kerby.tk.tealeaf.bem.BemItem;
import usa.kerby.tk.tealeaf.bem.BemJson;

/**
 * @author Trevor Kerby
 * @since Jul 20, 2020
 */
public class TealeafSerializer implements JsonbSerializer<TealeafEntity> {

	@Override
	public void serialize(TealeafEntity obj, JsonGenerator generator, SerializationContext ctx) {
		this.writeProps(obj.getProps(), generator, ctx);
		this.writeLinks(obj.getLinks(), generator, ctx);
		this.writeRequest(obj.getCacheableRequest(), generator, ctx);
		this.writeResponse(obj.getCacheableResponse(), generator, ctx);
		this.writeEmbedded(obj.getEmbedded(), generator, ctx);
		this.writeBem(obj, generator, ctx);
	}

	private void writeBem(TealeafEntity obj, JsonGenerator generator, SerializationContext ctx) {
		// if (obj.getBem().size() == 1) {
		// generator.writeStartObject("_bem");
		// if (obj.getBem(0).isReference()) {
		// generator.write("$ref", obj.getBem(0).getPath().getPath());
		// }
		// else {
		// ctx.serialize((BemJson) obj.getBem(0).getBemJson(), generator);
		// }
		// generator.writeEnd();
		// }
		// else if (obj.getBem().size() > 1) {
		generator.writeStartArray("_bem");
		for (BemItem bem : obj.getBem()) {
			generator.writeStartObject();
			if (bem.isReference()) {
				generator.write("$ref", bem.getPath().getPath());
			} else {
				ctx.serialize((BemJson) bem.getBemJson(), generator);
			}
			generator.writeEnd();
		}
		generator.writeEnd();
		// }
	}

	private void writeProps(Map<String, PathDecorator<Object>> props, JsonGenerator generator,
			SerializationContext ctx) {
		if (props != null && !props.isEmpty()) {
			for (Entry<String, PathDecorator<Object>> entry : props.entrySet()) {
				PathDecorator<Object> pathedHyperlink = entry.getValue();
				ctx.serialize(entry.getKey(), pathedHyperlink.getValue(), generator);
			}
		}

	}

	private void writeEmbedded(Map<LinkRelation, List<HalRepresentation>> embedded, JsonGenerator generator,
			SerializationContext ctx) {
		if (embedded != null && !embedded.isEmpty()) {
			generator.writeStartObject("_embedded");
			for (Entry<LinkRelation, List<HalRepresentation>> entry : embedded.entrySet()) {
				generator.writeStartArray(entry.getKey().toString());
				for (HalRepresentation child : entry.getValue()) {
					generator.writeStartObject();
					this.serialize((TealeafEntity) child, generator, ctx);
					generator.writeEnd();
				}
				generator.writeEnd();
			}
			generator.writeEnd();
		}
	}

	/**
	 * 
	 */
	private void writeLinks(Map<LinkRelation, PathDecorator<Hyperlink>> links, JsonGenerator generator,
			SerializationContext ctx) {
		if (links != null && !links.isEmpty()) {
			generator.writeStartObject("_links");
			for (Entry<LinkRelation, PathDecorator<Hyperlink>> entry : links.entrySet()) {
				PathDecorator<Hyperlink> pathedHyperlink = entry.getValue();
				generator.writeStartObject(entry.getKey().toString());
				ctx.serialize(pathedHyperlink.getValue(), generator);
				generator.writeEnd();
			}
			generator.writeEnd();
		}

	}

	/**
	 * @param cacheableRequest
	 * @param generator
	 * @param ctx
	 */
	private void writeRequest(HttpRequest cacheableRequest, JsonGenerator generator, SerializationContext ctx) {
		if (cacheableRequest != null) {
			generator.writeStartObject("_request");
			ctx.serialize(cacheableRequest, generator);
			generator.writeEnd();
		}
	}

	/**
	 * @param cacheableResponse
	 * @param generator
	 * @param ctx
	 */
	private void writeResponse(HttpResponse cacheableResponse, JsonGenerator generator, SerializationContext ctx) {
		if (cacheableResponse != null) {
			generator.writeStartObject("_response");
			ctx.serialize(cacheableResponse, generator);
			generator.writeEnd();
		}
	}

}
