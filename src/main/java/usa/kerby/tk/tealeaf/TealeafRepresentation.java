package usa.kerby.tk.tealeaf;

import usa.kerby.tk.jchal.ChalRepresentation;

/**
 * @author Trevor Kerby
 * @since Aug 6, 2020
 */
public interface TealeafRepresentation extends ChalRepresentation {

}
